const express = require("express");
const app = express();
const router = express.Router();
const cors = require("cors");
const path = require("path");
const bodyParser = require("body-parser");
const serverless = require("serverless-http");
const fetch = require("node-fetch");
const {exec} = require('child_process');

router.use(bodyParser.text());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use(express.static(path.join(__dirname, "public")));

router.get("/negotiations", async (req, res) => {
	try {
		const metrics = await fetch("https://bitnuvem.com/api/BTC/ticker")
			.then(response => response.json())
			.then(data => {
				const {ticker: { date, high, low, vol, last, buy, sell } } = data;
				const metrics = {
					high,
					low,
					buy,
					sell
				};
				res.json({ ...metrics });
			});
		return {
			...metrics
		}
	} catch (error) {
		res.sendStatus(500);
	}
});

router.get("/offers", async (req, res) => {
	try {
		const offers = await fetch("https://bitnuvem.com/api/BTC/orderbook")
			.then(response => response.json())
			.then(data => {
				const { bids, asks } = data;
				return {
					bids,
					asks
				}
			});
			res.json({
				...offers
			})
	} catch (error) {
		res.sendStatus(500);
	}
});

router.get("/wallet", async (req, res) => {
	try {
		exec("python3 getBitnuvemBalance.py", (error, stdout, stderr) => {
			if (error) {
				res.sendStatus(500);
			}
			if (stderr) {
				res.sendStatus(500);
			}
			const response = JSON.parse(stdout.replace(/'/g, '"'));
			res.json({
				data: {
					status: 200,
					statusText: 'success',
					data: {
						...response
					}
				}
			});
	});
	} catch (error) {
		res.sendStatus(500);
	}
})

app.use(router);
module.exports.handler = serverless(app);
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
