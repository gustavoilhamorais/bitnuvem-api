# BitNuvem Node JS API

## Description
Este repositório consiste em uma API feita com __Javascript e Express JS__ (enter outros módulos) que tem como objetivo **tratar e retornar** as respostas das rotas disponíveis na **API da plataforma** de compra e venda de __bitcoins__, _**Bitnuvem**_.

## Scripts
Na pasta raíz do projeto, execute:

#### Para instalar o projeto:
> npm install

#### Para executar o servidor:
> npm start

#### Para executar o servidor em modo desenvolvimento:
> npm run dev

- Observação:
  - o modo de desenvolvimento consiste em iniciar o servidor através da biblioteca nodemon.
  - a tarefa principal do nodemon é monitorar os arquivos do projeto e __restartar__ o servidor a cada alteração salva, ou seja, basicamente a cada '__ctrl+s__'.

Sinta-se a vontade para contribuir implementando novas rotas ou aprimorando o código já existente.
