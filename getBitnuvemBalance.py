import requests
import time
import hmac
import hashlib
import json

api_key = ''
API_SECRET = '' 
url = 'https://bitnuvem.com/tapi/'
endpoint = 'balance'
timestamp = int(time.time())
request_body = 'timestamp=' + str(timestamp)

signature = hmac.new(bytes(API_SECRET , 'latin-1'), msg = bytes(request_body , 'latin-1'), digestmod = hashlib.sha256).hexdigest()

api_post = {
	'api_key': api_key,
	'request_body': request_body,
	'signature': signature
}

response = requests.post(url + endpoint, api_post)
responseJSON = response.json()

print(responseJSON)